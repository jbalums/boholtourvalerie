<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Pacifico" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="dns-prefetch" href="//use.fontawesome.com">
    <script src="https://use.fontawesome.com/d39139a3d8.js"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>Bohol Tour Valerie</title>
</head>
<body> 
    <nav class="navbar navbar-expand navbar-dark bg-primary py-1" style="z-index: 2;">  
        <div class="collapse navbar-collapse py-0">
            <div class="container">
                <ul class="navbar-nav flex-column flex-md-row flex-lg-row flex-xl-row">
                    <li class="nav-item mx-auto mx-md-0 mx-lg-0 mx-xl-0">
                        <span class="nav-link text-white py-0"><i class="fa fa-phone mr-1"></i> +63 948 735 1794</span>
                    </li> 
                    <li class="nav-item mx-auto mx-md-0 mx-lg-0 mx-xl-0">
                        <span class="nav-link text-white py-0"><i class="fa fa-envelope-o mr-1"></i> info@boholtourvalerie.com</span>
                    </li>  
                </ul> 
                
            </div>
        </div>
    </nav> 
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm py-0 main-nav">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="images/logo.png">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExample04"> 
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link text-uppercase px-4" href="index.html">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-uppercase px-4" href="tour_packages.html">Tour Packages</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-uppercase px-4" href="rental_services.html">Rental Services</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link text-uppercase px-4" href="contact_us.php">Contact Us</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="section">
        <div class="container-fluid px-0 bg-light">
            <nav aria-label="breadcrumb ">
                <ol class="breadcrumb container bg-transparent py-4 d-flex align-items-center">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                    <div class="btn btn-primary btn-sm ml-auto">INQUIRE NOW</div>
                </ol>
            </nav>
        </div>
    </div>


    <?php
    if(isset($_POST['email'])){
        if($_POST['email']){
            $to = "baluma.joel91@gmail.com";
            $subject = $_POST['subject'];
            $txt = $_POST['subject'];
            $headers = "From: ".$_POST['email'].'['.$_POST['name'].']'.' | ['.$_POST['contact'].']';
        }
        if($_POST['email']){
            mail($to,$subject,$txt,$headers);
        }
    }
    ?>
    
    <div class="section">
        <div class="container pb-5 mb-5 ">
            <div class="row">
                <div class="col-md-12 text-center mb-5">
                    <h1 class="mt-5 mb-4" style="font-family: 'Pacifico', cursive;">Contact Us</h1>
                    <p class="text-center">GET IN TOUCH WITH US!</p>
                    <p class="text-center w-75 mx-auto m-0">We are happy to answer your questions. <br/>Fill out the form and we'll be in touch with you as soon as possible.</p>
                    <?php if(isset($_POST['email'])) { ?> 
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  <strong>Message Sent!</strong> Thank you for getting in touch!
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div> 
                    <?php } ?>
                </div>
                <div class="col-md-5 mx-auto">
                    <form method="POST" action=""> 
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email address">
                        </div>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Your name">
                        </div>
                        <div class="form-group">
                            <input type="text" name="contact" class="form-control" placeholder="Contact number">
                        </div>
                        <div class="form-group">
                            <input type="text" name="subject" class="form-control" placeholder="What's this about?">
                        </div>
                        <div class="form-group">
                            <textarea rows="3" class="form-control" name="message" placeholder="Message"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block mt-4">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <section class="section bg-light py-4" style="">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h6 class="font-weight-bold">BOHOL TOUR VALERIE</h6>
                </div>
                <div class="col-md-4">
                    <ul class="list-unstyled">
                        <li class="mt-1">CPG Avenue,</li>
                        <li class="mt-1">Tagbilaran City</li>
                        <li class="mt-1">Bohol, 6300, Philippines</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-unstyled">
                        <li class="mt-1"><i class="fa fa-phone text-primary mr-1"></i> +63 948 735 1794</li>
                        <li class="mt-1"><i class="fa fa-phone text-light mr-1"></i> +63 965 566 0872</li>
                        <li class="mt-1">&nbsp;</li>
                        <li class="mt-1"><i class="fa fa-envelope text-primary mr-1"></i> info@booltourvalerie.com</li>
                    </ul>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </section>

    <footer class="footer mt-auto py-3 bg-dark">
        <div class="container">
                <ul class="navbar-nav flex-column flex-md-row flex-lg-row flex-xl-row">
                    <li class="nav-item">
                        <span class="text-white">Copyright © 2019 Bohol Tour Valerie. All rights reserved.</span>
                    </li>
                    <li class="nav-item mr-auto ml-auto mr-md-0 mr-lg-0 mr-xl-0">
                        <span class="nav-link text-white py-0 d-inline-flex align-items-center justify-content-center">
                            <i class="fa fa-facebook mr-2 fs-17"></i>
                            <i class="fa fa-twitter mx-2 fs-17"></i>
                            <i class="fa fa-instagram mx-2 fs-17"></i>
                            <i class="fa fa-youtube mx-2 fs-17"></i>
                        </span>
                    </li> 
                </ul> 
        </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>